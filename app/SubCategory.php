<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = ['category_id','name'];

    protected $table = 'sub_categories';

    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }

    public function product(){
        return $this->hasMany('App\Product','sub_category_id');
    }
}
