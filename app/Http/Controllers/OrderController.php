<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Stock;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = Order::with('product')->get();
        return view('order.index', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = Product::get();
        return view('order.create', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order();
        $order->name = $request->name;
        $order->email = $request->email;
        $order->product_id = $request->product_id;
        $order->quantity = $request->quantity;
        $order->save();

        $stock = Stock::where('product_id',$request->product_id)->first();
        if ($stock != null) {
            $stock->quantity = $stock->quantity - $request->quantity;
            $stock->save();
        }

        $data = array('name'=>"Biplab Baniya");

        Mail::send(['text'=>'mail'], $data, function($message) use ($request) {
            $message->to($request->email, 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
            $message->from($request->email,'Biplab Baniya');
        });
        return redirect()->route('order.index');
    }

    public function generate(){
        $data = Order::with('product')->get();
        $pdf = PDF::loadView('myPDF', compact('data'));
        return $pdf->download('sample_report.pdf');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
