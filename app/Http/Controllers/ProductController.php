<?php

namespace App\Http\Controllers;

use App\Category;
use App\Image;
use App\Product;
use App\SubCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::with('subcategory.category')->paginate(2);
        return view('product.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::get();
        $subcategory = SubCategory::with('category')->get();
        return view('product.create', compact('subcategory','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $product = new Product();
            $product->sub_category_id = $request->sub_category_id;
            $product->name = $request->name;
            $product->price = $request->price;
            $product->save();

            foreach ($request->image as $att){
                $image = new Image();
                $image->product_id = $product->id;
                $filename  = $att;
                $destpath  = "images/sample/";
                $imagename = uniqid() . "-" . $filename->getClientOriginalName();
                $filename->move($destpath, $imagename);
                $photo = $destpath . $imagename;
                $newfile = $photo;
                $image->image = $newfile;
                $image->save();
            }
            return redirect()->route('product.index');
        }
        catch (\Exception $e){
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = Image::where('product_id',$id)->get();
        return view('product.show', compact('image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
