<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['sub_category_id','name','price'];

    protected $table = "products";

    public function subcategory(){
        return $this->belongsTo('App\SubCategory','sub_category_id');
    }

    public function image(){
        return $this->hasMany('App\Image', 'product_id');
    }

    public function stock(){
        return $this->hasMany('App\Stock','product_id');
    }

    public function order(){
        return $this->hasMany('App\Order','product_id');
    }
}
