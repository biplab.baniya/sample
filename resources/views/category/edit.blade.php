@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Category Edit</div>

                    <div class="card-body">
                        <form method="post" action="{{route('category.update', $category->id)}}">
                            @csrf
                            @method('PUT')
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" value="{{$category->name}}" required>
                            <label>Remarks</label>
                            <input type="text" class="form-control" name="remarks" value="{{$category->remarks}}" required>
                            <input type="submit" name="Submit">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
