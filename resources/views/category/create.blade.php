@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Category Add</div>

                    <div class="card-body">
                        <form method="post" action="{{route('category.store')}}">
                            @csrf
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" required>
                            <label>Remarks</label>
                            <input type="text" class="form-control" name="remarks" required>
                            <input type="submit" name="Submit">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
