@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Images</div>

                    <div class="card-body">
                        @foreach($image as $images)
                            <img src="{{asset($images->image)}}" style="height: 400px" class="form-control">
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
