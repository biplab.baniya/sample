@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add Product</div>

                    <div class="card-body">
                        <form method="post" action="{{route('product.store')}}" enctype="multipart/form-data" id="form-product">
                            @csrf
                            <label>Category</label>
                            <select class="form-control" name="sub_category_id" id="category">
                                @foreach($category as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                            <label>Sub Category</label>
                            <select class="form-control" name="sub_category_id" id="subcat">

                            </select>
                            <label>Product Name</label>
                            <input type="text" class="form-control" name="name" required>
                            <label>Price</label>
                            <input type="number" class="form-control" name="price">
                            <label>Images</label>
                            <input type="file" class="form-control" name="image[]" placeholder="images" multiple>
                            <input type="submit" name="Submit">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#category').on('change', function () {
                var vals = document.getElementById('category').value;
                $('#subcat').empty();

                <?php foreach($subcategory as $row): ?>
                    if (<?php echo $row->category->id; ?> == vals) {

                    var sel = document.getElementById('subcat');
                    // create new option element
                    var opt = document.createElement('option');

                    opt.appendChild( document.createTextNode('<?php echo $row->name ?>') );

                    opt.value = <?php echo $row->id ?>;

                    sel.appendChild(opt);
                    }
                <?php endforeach ?>
            })
        })
    </script>

@endsection
