@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Product</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <a href="{{route('product.create')}}" class="btn btn-primary">Create Product</a>
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Image</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($product as $row)
                                <tr>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->price}}</td>
                                    <td><a href="{{route('product.show', $row->id)}}" class="btn btn-primary">See Image</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$product->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
