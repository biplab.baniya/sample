@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Sub Category</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <a href="{{route('subcategory.create')}}" class="btn btn-primary">Create Sub Category</a>
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>category</th>
                                <th>name</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($subcategory as $row)
                                <tr>
                                    <td>{{$row->category->name}}</td>
                                    <td>{{$row->name}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
