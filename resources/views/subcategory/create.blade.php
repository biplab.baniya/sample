@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add Sub Categort</div>

                    <div class="card-body">
                        <form method="post" action="{{route('subcategory.store')}}">
                            @csrf
                            <label>Category</label>
                            <select class="form-control" name="category_id">
                                @foreach($category as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                            <label>Sub Category Name</label>
                            <input type="text" class="form-control" name="name" required>
                            <input type="submit" name="Submit">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
