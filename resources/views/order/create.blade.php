@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add Order</div>

                    <div class="card-body">
                        <form method="post" action="{{route('order.store')}}">
                            @csrf
                            <label>Customer Name</label>
                            <input type="text" name="name" class="form-control">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control">
                            <label>Product</label>
                            <select class="form-control" name="product_id">
                                @foreach($product as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                            <label>Quantity</label>
                            <input type="integer" class="form-control" name="quantity" required>
                            <input type="submit" name="Submit">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
