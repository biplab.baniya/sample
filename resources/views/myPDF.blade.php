<!DOCTYPE html>
<html>
<head>
    <title>Hi</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>Customer Name</th>
                <th>Customer Email</th>
                <th>Product Name</th>
                <th>Product Price</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $row)
                <tr>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->product->name}}</td>
                    <td>{{$row->quantity}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
